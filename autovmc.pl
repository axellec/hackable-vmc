#!/usr/bin/perl

# L'objectif de ce script est dans un premier temps de ventiler
# en 'rechauffant' la maison
# (c) A. Apvrille - 2015

# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Getopt::Long;
use Sys::Syslog qw(:standard :macros);

# ---------------------------
my $weewx_dir = '/home/weewx'; # MODIFY THIS IF YOU INSTALLED WEEWX ELSEWHERE
my $vmc_dir = '/usr/share/vmc'; # Modify this if needed

my $pulse_indicator = "/var/tmp/vmc_pulse.archive"; # I personally put this on a RAM based filesystem
my $auto_indicator = "/var/tmp/vmc.auto";

# you usually don't need to change this
my $config_file = "$vmc_dir/autovmc.config";
my $debug = 0;

# ---------------------------
sub debug_log {
    my $message = shift;

    if ($debug) {
	print $message;
    }
}

# checks the mininum out temperature and max out humidity make sense
# if not DIES (closes the log).
sub check_config {
    my $minOutTemp = shift;
    my $maxOutHumidity = shift;

    debug_log("--> check_config()\n");
    # test for stupid values
    if ($minOutTemp < 5 || $minOutTemp > 40) {
	syslog(LOG_ERR, "Incorrect value for minOutTemp=$minOutTemp");
	closelog();
	die "Incorrect value for minOutTemp";
    }
    if ($maxOutHumidity < 20 || $maxOutHumidity > 95) {
	syslog(LOG_ERR, "Incorrect value for maxOutHumidity=$maxOutHumidity");
	closelog();
	die "Incorrect value for maxOutHumidity";
    }
    debug_log("<-- check_config()\n");
}

# Reads the autovmc configuration file
# Example of config file:
# minOutTemp=10
# maxOutHumidity=80
#
sub read_config {
    my $filename = shift;
    debug_log("--> read_config(): $filename\n");
    open( my $fh, $filename ) or die "Could not open config file: $filename: $!";
    my $minOutTemp;
    my $maxOutHumidity;
    
    while (my $row = <$fh>) {
	chomp($row);
	my ($tag, $value) = split(/=/, $row);
	$tag =~ s/ //g;
	$value =~ s/ //g;
	debug_log("\ttag=$tag, value=$value\n");
	if ($tag =~ /minOutTemp/i) {
	    $minOutTemp = $value;
	    debug_log("\tminOutTemp=$value\n");
	}
	if ($tag =~ /maxOutHumidity/i) {
	    $maxOutHumidity = $value;
	    debug_log("\tmaxOutHumidity=$value\n");
	}
	    
    }
    close($fh);

    debug_log("<-- read_config(): $minOutTemp, $maxOutHumidity\n");
    return $minOutTemp, $maxOutHumidity;
}

# reads data from the weather station and returns
# inner temperature, outer temperature and outer humidity
sub read_weather {
    # This retrieves the most recent entry in the database
    my $dateTime=`sqlite3 $weewx_dir/archive/weewx.sdb "select max(dateTime) from archive;"`;
    chomp($dateTime);

    # make sure dateTime is sound
    my $currentTime = time();
    print "weewxTime=$dateTime current=$currentTime\n";
    if ($currentTime - $dateTime > 10 * 60 * 60) {
	debug_log("Weewx time is obsolete");
	syslog(LOG_WARNING, "Last weewx measure too old: setting VMC low: current= $currentTime lastReport=$dateTime");
	system("$vmc_dir/vmctrl.pl --low");
	closelog();
	die "Obsolete weewx time!";
    }


    # This gets the temperature for that entry
    my $inTemp=`sqlite3 /home/weewx/archive/weewx.sdb "select inTemp from archive where dateTime=${dateTime};"`;

    my $outTemp = `sqlite3 /home/weewx/archive/weewx.sdb "select outTemp from archive where dateTime=${dateTime};"`;

    my $outHumidity = `sqlite3 /home/weewx/archive/weewx.sdb "select outHumidity from archive where dateTime=${dateTime};"`;
    chomp($inTemp);
    chomp($outTemp);
    chomp($outHumidity);

    if ($inTemp < -20 || $inTemp > 50) {
	syslog(LOG_ERR, "Inner temperature is wrong: $inTemp - setting VMC low");
	closelog();
	die "Something wrong with inner temperature: $inTemp";
    }

    if ($outTemp < -20 || $outTemp > 50) {
	syslog(LOG_ERR, "Outer temperature is wrong: $outTemp - setting VMC low");
	closelog();
	die "Something wrong with outer temperature: $outTemp";
    }

    if ($outHumidity < 0 || $outHumidity > 100) {
	syslog(LOG_ERR, "Outer humidity is wrong: $outHumidity - setting VMC low");
	closelog();
	die "Something wrong with outer humidity: $outHumidity";
    }
    

    debug_log("Inner temperature: $inTemp\n");
    debug_log("Outer temperature: $outTemp\n");
    debug_log("Outer humidity:    $outHumidity\n");
    return $inTemp, $outTemp, $outHumidity;
}

# Algorithm to warm up the house
#
# si on donne une commande via le web, elle predomine sur le programme.
# si sur le web on est en mode "auto" (n'existe pas actuellement),
# alors
# 	si temp exterieure > minTemp ET humidite exterieure < maxHumid
# 	alors
# 		si temp ext > temp int
# 			vitesse 2
# 		sinon
# 			vitesse 1
# 		fin si
# 	fin si
# fin si
sub warm_house {
    my $inTemp = shift;
    my $outTemp = shift;
    my $outHumidity = shift;
    my $minOutTemp = shift;
    my $maxOutHumidity = shift;

    debug_log("--> warm_house(): inTemp=$inTemp C\n");
    debug_log("\toutTemp       =$outTemp C,\n");
    debug_log("\toutHumidity   =$outHumidity\n");
    debug_log("\tminOutTemp    =$minOutTemp C\n");
    debug_log("\tmaxOutHumidity=$maxOutHumidity\n");

    if (-e $pulse_indicator || (! -e $auto_indicator)) {
	syslog(LOG_NOTICE, "Manual mode or pulse detected - autovmc won't do anything");
	debug_log("\tManual mode. Don't do anything\n");
    } else {
	debug_log("\tAutomatic mode: let's do something smart (perhaps)\n");
	if ($outTemp > $minOutTemp && $outHumidity < $maxOutHumidity) {
	    if ($outTemp > $inTemp) {
		debug_log("\tHeating up the house with speed 2\n");
		syslog(LOG_INFO, "Graph info: speed=2 outTemp=$outTemp, inTemp=$inTemp, outHumidity=$outHumidity");
		syslog(LOG_INFO, "Heating up the house with speed 2: outTemp=$outTemp, inTemp=$inTemp, outHumidity=$outHumidity");
		system("$vmc_dir/vmctrl.pl --high");
	    } else {
		debug_log("\tHeating up the house with speed 1\n");
		syslog(LOG_INFO, "Graph info: speed=1 outTemp=$outTemp, inTemp=$inTemp, outHumidity=$outHumidity");
		syslog(LOG_INFO, "Aerating with speed 1: outTemp=$outTemp, inTemp=$inTemp, outHumidity=$outHumidity");
		system("$vmc_dir/vmctrl.pl --low");
	    }
	} else {
	    debug_log("\tIt's cold or rainy. Stop the CMV.\n");
	    syslog(LOG_INFO, "Graph info: speed=0 outTemp=$outTemp, inTemp=$inTemp, outHumidity=$outHumidity");
	    syslog(LOG_INFO, "Stopping the CMV: outTemp=$outTemp, outHumidity=$outHumidity");
	    system("$vmc_dir/vmctrl.pl --stop");
	}
    }
    debug_log("<-- warm_house()\n");
}

# si temp exterieure < temp interieure
# alors
#     vitesse 2
# else
#     vitesse 1
# fin si
sub cool_house {
    my $inTemp = shift;
    my $outTemp = shift;
    
    debug_log("--> cool_house(): inTemp=$inTemp C\n");
    debug_log("\toutTemp       =$outTemp C,\n");
    
    if (-e $pulse_indicator || (! -e $auto_indicator)) {
	syslog(LOG_NOTICE, "Manual mode or pulse detected - autovmc won't do anything");
	debug_log("\tManual mode. Don't do anything\n");
    } else {
	debug_log("\tAutomatic mode: let's do something smart (perhaps)\n");
	if ($outTemp < $inTemp) {
	    debug_log("\tCooling house with speed 2\n");
	    syslog(LOG_INFO, "Graph info: speed=2 outTemp=$outTemp, inTemp=$inTemp, outHumidity=N/A");
	    syslog(LOG_INFO, "Cooling house with speed 2: outTemp=$outTemp, inTemp=$inTemp");
	    system("$vmc_dir/vmctrl.pl --high");
	} else {
	    debug_log("\tIt's warmer outside, let's aerate\n");
	    syslog(LOG_INFO, "Graph info: speed=1 outTemp=$outTemp, inTemp=$inTemp, outHumidity=N/A");
	    syslog(LOG_INFO, "Cooling house with Speed 1: outTemp=$outTemp, inTemp=$inTemp");
	    system("$vmc_dir/vmctrl.pl --low");
	}
    }
}

# vitesse 1, sauf si humidite excessive 
sub neutral_house {
    my $outHumidity = shift;
    my $maxOutHumidity = shift;

    debug_log("--> neutral_house(): outHumidity=$outHumidity\n");
    debug_log("\tmaxOutHumidity: $maxOutHumidity\n");
    
    if (-e $pulse_indicator || (! -e $auto_indicator)) {
	syslog(LOG_NOTICE, "Manual mode or pulse detected - autovmc won't do anything");
	debug_log("\tManual mode. Don't do anything\n");
    } else {
	debug_log("\tAutomatic mode: let's do something smart (perhaps)\n");
	if ($outHumidity < $maxOutHumidity) {
	    debug_log("\tCooling house with speed 1\n");
	    syslog(LOG_INFO, "Cooling house with speed 1: outHumidity=$outHumidity");
	    system("$vmc_dir/vmctrl.pl --low");
	} else {
	    debug_log("\tToo much humidity, let's stop\n");
	    syslog(LOG_INFO, "Stopping the CMV: outHumidity=$outHumidity");
	    system("$vmc_dir/vmctrl.pl --stop");
	}
    }
    
}


sub usage {
    print "./autovmc.pl [--verbose] [--config file]\n";
    print "             [--minTemp value] [--maxHumidity value]\n";
    print "             [--cool] [--neutral]\n";
    print "--config file\n";
    print "         the configuration file contains:\n";
    print "         values for the minimum outside temperature\n";
    print "         before we do anything\n";
    print "         and value for the maximum outside humidity\n";
    print "         after which we won't aerate\n";
    print "Example of file:\n";
    print "minOutTemp=10\nmaxOutHumidity=80\n";
    print "--minTemp value\n";
    print "         sets the minimum outside temperature in Celsius\n";
    print "         and supersedes values of the config file\n";
    print "--maxHumidity value\n";
    print "         sets the maximum outside humidity\n";
    print "         and supersedes values of the config file\n";
    print "--cool   cool the house instead of warm it!\n";
    print "--neutral speed 1 all the time except if very humid\n";
    die("Quitting");

}

# --------- MAIN ----------------------------------------
my $minTemp;
my $maxHumidity;
my $cool;
my $neutral;

GetOptions ("verbose" => \$debug,
	    "config=s" => \$config_file,
	    "minTemp=s" => \$minTemp,
	    "cool" => \$cool,
	    "neutral" => \$neutral,
	    "maxHumidity=s" => \$maxHumidity
     )
    or usage();

# open syslog
openlog('autovmc', 'pid', LOG_USER);
#syslog(LOG_INFO, "Starting autovmc");
debug_log("Starting autovmc\n");

# read configuration file, or supersede with arguments
my ($minOutTemp, $maxOutHumidity) = read_config($config_file);
if (defined $minTemp) {
    debug_log("Superseding $config_file with minOutTemp=$minTemp\n");
    $minOutTemp = $minTemp;
}
if (defined $maxHumidity) {
    debug_log("Superseding $config_file with maxOutHumidity=$maxHumidity\n");
    $maxOutHumidity = $maxHumidity;
}
check_config($minOutTemp, $maxOutHumidity);

# read current weather data
my ($intemp, $outtemp, $outhumidity) = read_weather();

if (defined $cool) {
    cool_house($intemp, $outtemp);
} else {
    if (defined $neutral) {
	neutral_house($outhumidity, $maxOutHumidity);
    } else {
	# warm up the house if possible
	warm_house($intemp, $outtemp, $outhumidity, $minOutTemp, $maxOutHumidity);
    }
}

# close the log and don't return anything special
debug_log("Quitting autovmc\n");
#syslog(LOG_INFO, "Exiting autovmc");
closelog();

exit(1);
